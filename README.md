# Delivery Component Flux Setup

This repository contains resources to deploy the [delivery group's](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/) customized Flux components. The components are defined or referenced under `components` and they can be customized per different clusters under `overlays`.

## Flux Components

Flux is a continuous delivery tool for keeping our Kubernetes clusters in sync with our git repositories.

Read more on the official Flux documentation: https://fluxcd.io/flux/

### Reference components

If the Flux component is a shared component across multiple [tenants](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/tenants), it should be created in a shared components repository ([Components repository](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components)) then referenced from it via a [`GitRepository` resource](https://fluxcd.io/flux/components/source/gitrepositories/).

The git repositories (`GitRepository` resources) are stored per cluster and tenant level under the [Clusters](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters/-/tree/main/) repository. For example, the `GitRepository` resource `flux-components` referenced in [`flux-components-gitlab`](./components/gitlab/flux-components-gitlab.yaml) is fetched from [the Components repository](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components) and is defined in [the Clusters repository](https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/clusters/-/blob/main/gke/gitlab-pre/us-east1/pre-gl-gke-2/components-base.yaml).

Changes specific to the `delivery` group for _all_ the clusters' deployments of a component should go into this `components` folder. The `overlays` folder is more suited for any changes need to be made on a more granular level.

## Flux Overlays

Overlays are used to customize the resources more granularly: per-environment, per-zone, and per-cluster. The `kustomization.yaml` files are referenced in the [`overlays.yaml` file](./overlays.yaml). For example, changes that need to be made specific to `pre-gl-gke-2` cluster for `gitlab` component should be made under [`overlays/gke/gitlab-pre/us-east1/pre-gl-gke-2/gitlab/` folder](./overlays/gke/gitlab-pre/us-east1/pre-gl-gke-2/gitlab/).

Refer to [Flux documentation on ways of structuring your repositories](https://fluxcd.io/flux/guides/repository-structure/).

## Local testing

Testing changes and diff can be done using the [Flux CLI](https://fluxcd.io/flux/cmd/).

1. Install [Flux CLI](https://fluxcd.io/flux/cmd/)
2. (If testing a change made in a referenced `GitRepository`) Push changes made in the referenced repository to a branch
3. Locally connect to the cluster you wish to test on or deploy to via `glsh` or `gcloud` CLI. Refer to the documentation on how to [set up Kubernetes API Access](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#kubernetes-api-access).

    For `pre-gl-gke-2`, we cannot connect to that cluster using `glsh`. Run the following command on terminal instead of `glsh` to access that cluster.
     ```
     gcloud container clusters get-credentials pre-gl-gke-2 --region us-east1 --project gitlab-pre
     ```
4. (If testing a change made in a referenced `GitRepository`) Manually modify the referenced `GitRepository` resource on the cluster to point to that new branch (temporarily). For example:
    ```
    --- # Example GitRepository for Components repository
    apiVersion: source.toolkit.fluxcd.io/v1
    kind: GitRepository
    metadata:
    name: flux-components
    namespace: flux-system
    spec:
    interval: 1m0s
    ref:
        branch: main # -> my-feature-branch
    secretRef:
        name: flux-system
    url: https://gitlab.com/gitlab-com/gl-infra/k8s-mgmt/components.git
     ```
5. Run `flux diff kustomization` or `flux create kustomization` on the component folder (e.g. `flux diff kustomization istio --path ./components/istio`)
6. (If testing a change made in a referenced `GitRepository`) Change back the `GitRepository` resource from step 4 to point to `main` branch
